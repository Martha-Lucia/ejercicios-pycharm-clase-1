#Programa para presentar el valor de la expresiòn y a que tipo pertenece.
#__autora__="Martha Cango"
#__email__="martha.cango@unl.edu.ec"
#Asume que ejecutamos las siguientes sentencias de asignaciòn.
ancho=17
alto=12.0
#Para cada una de las expresiones siguientes,escribe el valor de la expresiòn y el
# tipo(del valor de la expresiòn).
print("17/2=" ,ancho/2)
print(type(ancho/2))
print("17/2.0=" ,ancho/2.0)
print(type(ancho/2.0))
print("12.0/3=" ,alto/3)
print(type(alto/3))
print("1+2*5=" ,1+2*5)
print(type(1+2*5))











